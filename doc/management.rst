Management
==========

.. toctree::
    :maxdepth: 1

    contributing
    authors
    history
    badges <badges/listing>
