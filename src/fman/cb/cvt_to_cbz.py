"""Script used to convert files to cbz.
"""
from itertools import chain
from pathlib import Path
from shutil import rmtree
from subprocess import check_call

from .editing import make_cbz

tmp_fld = Path(".tmp_fld")
trash_fld = Path(".trash")


def cvt(pth, dst, rm_src=False):
    print("cbz", pth)

    # clean tmp_fld
    rmtree(tmp_fld)
    tmp_fld.mkdir()

    # extract to tmp_fld
    if pth.suffix in (".cbz", ".cbr", ".rar"):
        cmd = f'7z e -o{tmp_fld} "{pth}"'
    elif pth.suffix == ".pdf":
        cmd = f'pdfimages "{pth}" {tmp_fld}/page'
    else:
        raise UserWarning(f"unrecognized format for {pth}")

    check_call(cmd, shell=True)

    # move file to trash
    if rm_src:
        pth.rename(trash_fld / pth.name)

    # create archive
    cbz_tgt = pth.with_suffix(".cbz")
    if dst is not None:
        cbz_tgt = dst / cbz_tgt.name

    make_cbz(tmp_fld, cbz_tgt)


def cvt_files(pth, dst=None, rm_src=False):
    """Convert files in current directory or whose names have been
    passed on the command line.

    Args:
      pth (Path): Path to format.
      dst (None|Path): Path to write new file (-if None, overwrite)
      rm_src (bool): Whether to remove initial file

    Returns:
      (None)
    """
    if not tmp_fld.exists():
        tmp_fld.mkdir()

    if not trash_fld.exists():
        trash_fld.mkdir()

    if dst is None:
        assert rm_src  # original file will be overwritten
    else:
        dst.mkdir(exist_ok=True, parents=True)

    if pth.is_dir():
        for sub_pth in sorted(chain(pth.glob("*.pdf"),
                                    pth.glob("*.cbz"),
                                    pth.glob("*.cbr"),
                                    pth.glob("*.rar"))):
            cvt(sub_pth, dst, rm_src)
    else:
        cvt(pth, dst, rm_src)
