"""Script used to convert image to low definition.
"""
from io import BytesIO
from pathlib import Path
from shutil import rmtree
from zipfile import ZipFile

import PIL
from PIL import Image

from .editing import make_cbz
from .standard import img_exts

tmp_fld = Path(".tmp_fld")
trash_fld = Path(".trash")

screen_height = 1920  # [pix]


def cvt(cbz_src, dst, rm_src=False):
    print("ld", cbz_src, dst, rm_src)

    # clean tmp_fld
    rmtree(tmp_fld)
    tmp_fld.mkdir()

    # open file and extract images
    with ZipFile(cbz_src, 'r') as zf:
        pages = [info.filename for info in zf.infolist()
                 if not info.is_dir()
                 and info.filename.split(".")[-1].lower() in img_exts]
        pages.sort()

        imgs = []
        for i, page in enumerate(pages):
            img_data = zf.read(page)
            try:
                img = Image.open(BytesIO(img_data))
            except PIL.UnidentifiedImageError:
                print("error", page)
                if i < len(pages) - 1:  # last page might be shitty anyway
                    raise
                else:
                    pth_err = Path("page_err")
                    pth_err.mkdir(exist_ok=True)
                    with open(pth_err / f"{cbz_src.name[:-4]}_page{i:04d}.jpg", "wb") as fhw:
                        fhw.write(img_data)

            imgs.append(img)

    # analyse images
    heights = []
    for i, img in enumerate(imgs):
        w, h = img.size
        if h > w:
            heights.append(h)

    sca = None
    if len(heights) > 0:
        h_mean = sum(heights) / len(heights)

        if h_mean > screen_height:
            print("need resize")
            sca = screen_height / h_mean

    for i, img in enumerate(imgs):
        try:
            if sca is not None:
                img = img.resize((int(img.size[0] * sca), int(img.size[1] * sca)), Image.LANCZOS)

            if img.mode != 'RGB':
                img = img.convert("RGB")

            img.save(tmp_fld / f"page_{i:04d}.jpg")
        except OSError:
            print("error page", i)
            raise

    # move file to trash
    if rm_src:
        cbz_src.rename(trash_fld / cbz_src.name)

    # create archive
    if dst is None:
        cbz_tgt = cbz_src
    else:
        cbz_tgt = dst / cbz_src.name

    make_cbz(tmp_fld, cbz_tgt)


def cvt_files(pth, dst=None, rm_src=False):
    """Convert files in current directory or whose names have been
    passed on the command line.

    Args:
      pth (Path): Path to format.
      dst (None|Path): Path to write new file (-if None, overwrite)
      rm_src (bool): Whether to remove initial file

    Returns:
      (None)
    """
    if not tmp_fld.exists():
        tmp_fld.mkdir()

    if not trash_fld.exists():
        trash_fld.mkdir()

    if dst is None:
        assert rm_src  # original file will be overwritten
    else:
        dst.mkdir(exist_ok=True, parents=True)

    if pth.is_dir():
        for sub_pth in pth.glob("*.cbz"):
            cvt(sub_pth, dst, rm_src)
    else:
        cvt(pth, dst, rm_src)
