========================
fman
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/fman/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/fman/0.3.0/

.. image:: https://revesansparole.gitlab.io/fman/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/fman

.. image:: https://revesansparole.gitlab.io/fman/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/fman/

.. image:: https://badge.fury.io/py/fman.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/fman

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/revesansparole/fman/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/revesansparole/fman/commits/main

.. |main_coverage| image:: https://gitlab.com/revesansparole/fman/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/revesansparole/fman/commits/main

prod: |prod_build|_ |prod_coverage|_

.. |prod_build| image:: https://gitlab.com/revesansparole/fman/badges/prod/pipeline.svg
.. _prod_build: https://gitlab.com/revesansparole/fman/commits/prod

.. |prod_coverage| image:: https://gitlab.com/revesansparole/fman/badges/prod/coverage.svg
.. _prod_coverage: https://gitlab.com/revesansparole/fman/commits/prod
.. #}

file manager

